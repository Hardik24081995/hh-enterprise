package com.HHEnterprise.app.dialog

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LifecycleOwner
import com.HHEnterprise.app.R
import com.HHEnterprise.app.extention.getCurrentDate
import com.HHEnterprise.app.extention.getValue
import com.HHEnterprise.app.extention.showDateTimePicker
import com.HHEnterprise.app.extention.showNextFromStartDateTimePicker
import com.HHEnterprise.app.network.AutoDisposable
import com.HHEnterprise.app.utils.BlurDialogFragment
import com.HHEnterprise.app.utils.SessionManager
import com.HHEnterprise.app.utils.TimeStamp
import kotlinx.android.synthetic.main.dialog_date_filter.*


class DateFilterDailog(context: Context) : BlurDialogFragment(), LifecycleOwner {
    private val autoDisposable = AutoDisposable()
    private lateinit var session: SessionManager


    companion object {
        private lateinit var listener: onItemClick
        fun newInstance(
                context: Context,
                listeners: onItemClick
        ): DateFilterDailog {
            this.listener = listeners
            return DateFilterDailog(context)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_Dialog_Custom)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        session = SessionManager(requireContext())
        autoDisposable.bindTo(this.lifecycle)
        return inflater.inflate(R.layout.dialog_date_filter, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateData()
        dialog?.setCancelable(true)
        dialog?.setCanceledOnTouchOutside(true)
        btnSubmit.setOnClickListener {
            listener.onItemCLicked(edtStartDate.getValue(), edtEndDate.getValue())
            dismissAllowingStateLoss()
        }
        btn_close.setOnClickListener {
            dismissAllowingStateLoss()
        }

        edtStartDate.setText(TimeStamp.getStartDateRange())
        edtEndDate.setText(getCurrentDate())

        edtStartDate.setOnClickListener { showDateTimePicker(requireActivity(), edtStartDate) }

        edtEndDate.setOnClickListener { showDateTimePicker(requireActivity(), edtEndDate) }


        edtEndDate.setOnClickListener {
            showNextFromStartDateTimePicker(
                    requireActivity(),
                    edtEndDate,
                    edtStartDate.getValue()
            )
        }

        edtStartDate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                edtEndDate.setText(edtStartDate.getValue())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
    }

    private fun populateData() {
        val bundle = arguments
        if (bundle != null) {
            /* val title = bundle.getString(Constant.TITLE)
             val text = bundle.getString(Constant.TEXT)
             txtTitle.text = title
             tvText.text = text*/
        }
    }

    interface onItemClick {
        fun onItemCLicked(strdate: String, enddate: String)
    }

    interface onDissmiss {
        fun onDismiss()
    }
}








