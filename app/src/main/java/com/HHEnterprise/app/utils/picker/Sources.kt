package com.HHEnterprise.app.utils.picker

enum class Sources {
    CAMERA, GALLERY, DOCUMENTS, CHOOSER
}