package com.HHEnterprise.app.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.HHEnterprise.app.R
import com.HHEnterprise.app.adapter.PenaltiAdapter
import com.HHEnterprise.app.extention.*
import com.HHEnterprise.app.interfaces.LoadMoreListener
import com.HHEnterprise.app.modal.LeadItem
import com.HHEnterprise.app.modal.PaneltyDataItem
import com.HHEnterprise.app.modal.PenaltyListModel
import com.HHEnterprise.app.network.CallbackObserver
import com.HHEnterprise.app.network.Networking
import com.HHEnterprise.app.network.addTo
import com.HHEnterprise.app.utils.Constant
import com.HHEnterprise.app.utils.SessionManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.reclerview_swipelayout.*
import kotlinx.android.synthetic.main.toolbar_with_back_arrow.*
import org.json.JSONException
import org.json.JSONObject

class PenaltyActivity : BaseActivity(), PenaltiAdapter.OnItemSelected {

    var adapter: PenaltiAdapter? = null
    private val list: MutableList<PaneltyDataItem> = mutableListOf()
    var page: Int = 1
    var hasNextPage: Boolean = true
    var leadItem: LeadItem? = null

    fun setupRecyclerView() {

        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        adapter = PenaltiAdapter(this, list, this)
        recyclerView.adapter = adapter

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_penaltys_list)

        txtTitle.setText(getString(R.string.nav_penalti))

        if (checkUserRole(session.roleData.data?.penlty?.isInsert.toString(), this)) {
            imgAdd.visible()
        }

        imgBack.setOnClickListener {
            finish()
        }

        imgAdd.setOnClickListener {
            goToActivity<AddPenaltyActivity>()

        }

        recyclerView.setLoadMoreListener(object : LoadMoreListener {
            override fun onLoadMore() {
                if (hasNextPage && !recyclerView.isLoading) {
                    progressbar.visible()
                    getPenaltyList(++page)
                }
            }
        })

        swipeRefreshLayout.setOnRefreshListener {
            page = 1
            list.clear()
            hasNextPage = true
            recyclerView.isLoading = true
            adapter?.notifyDataSetChanged()
            getPenaltyList(page)
        }
    }

    override fun onItemSelect(position: Int, data: PaneltyDataItem) {
        // goToActivity<EmployeeDetailActivity>()
    }


    fun getPenaltyList(page: Int) {
        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("PageSize", Constant.PAGE_SIZE)
            jsonBody.put("CurrentPage", page)
            jsonBody.put("CityID", session.getDataByKey(SessionManager.KEY_CITY_ID))


            result = Networking.setParentJsonData(
                    Constant.METHOD_GET_PANELTLY,
                    jsonBody
            )

        } catch (e: JSONException) {
            e.printStackTrace()
        }


        Networking
                .with(this)
                .getServices()
                .getPaneltyList(Networking.wrapParams(result))//wrapParams Wraps parameters in to Request body Json format
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : CallbackObserver<PenaltyListModel>() {
                    override fun onSuccess(response: PenaltyListModel) {
                        if (list.size > 0) {
                            progressbar.invisible()
                        }
                        swipeRefreshLayout.isRefreshing = false
                        list.addAll(response.data)
                        adapter?.notifyItemRangeInserted(
                                list.size.minus(response.data.size),
                                list.size
                        )
                        hasNextPage = list.size < response.rowcount!!

                        refreshData(getString(R.string.no_data_found), 1)
                    }

                    override fun onFailed(code: Int, message: String) {
                        if (list.size > 0) {
                            progressbar.invisible()
                        }
                        // showAlert(message)
                        showAlert(getString(R.string.show_server_error))
                        refreshData(message, code)
                    }

                }).addTo(autoDisposable)
    }


    private fun refreshData(msg: String?, code: Int) {
        recyclerView.setLoadedCompleted()
        swipeRefreshLayout.isRefreshing = false
        adapter?.notifyDataSetChanged()

        if (list.size > 0) {
            imgNodata.invisible()
            recyclerView.visible()
        } else {
            imgNodata.visible()
            if (code == 0)
                imgNodata.setImageResource(R.drawable.no_internet_bg)
            else
                imgNodata.setImageResource(R.drawable.nodata)
            recyclerView.invisible()
        }
    }

    override fun onResume() {
        page = 1
        list.clear()
        hasNextPage = true
        swipeRefreshLayout.isRefreshing = true
        setupRecyclerView()
        recyclerView.isLoading = true
        getPenaltyList(page)
        super.onResume()
    }


}