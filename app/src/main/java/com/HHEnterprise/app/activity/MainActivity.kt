package com.HHEnterprise.app.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.HHEnterprise.app.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}