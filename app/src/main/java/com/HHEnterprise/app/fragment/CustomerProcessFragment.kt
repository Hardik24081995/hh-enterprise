package com.HHEnterprise.app.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.HHEnterprise.app.R
import com.HHEnterprise.app.adapter.ProcessAdapter
import com.HHEnterprise.app.extention.invisible
import com.HHEnterprise.app.extention.showAlert
import com.HHEnterprise.app.extention.visible
import com.HHEnterprise.app.interfaces.LoadMoreListener
import com.HHEnterprise.app.modal.CustomerDataItem
import com.HHEnterprise.app.modal.ProcessListDataItem
import com.HHEnterprise.app.modal.ProcessListModal
import com.HHEnterprise.app.network.CallbackObserver
import com.HHEnterprise.app.network.Networking
import com.HHEnterprise.app.network.addTo
import com.HHEnterprise.app.utils.Constant
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.reclerview_swipelayout.*
import org.json.JSONException
import org.json.JSONObject


class CustomerProcessFragment() : BaseFragment(), ProcessAdapter.OnItemSelected {
    var customerDataIteam: CustomerDataItem? = null

    constructor(customerData: CustomerDataItem?) : this() {
        customerDataIteam = customerData
    }

    var adapter: ProcessAdapter? = null
    var list: MutableList<ProcessListDataItem> = mutableListOf()
    var page: Int = 1
    var hasNextPage: Boolean = true
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.reclerview_swipelayout, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.setLoadMoreListener(object : LoadMoreListener {
            override fun onLoadMore() {
                if (hasNextPage && !recyclerView.isLoading) {
                    progressbar.visible()
                    getProcessList(++page)
                }
            }
        })

        swipeRefreshLayout.setOnRefreshListener {
            page = 1
            list.clear()
            hasNextPage = true
            recyclerView.isLoading = true
            adapter?.notifyDataSetChanged()
            getProcessList(page)
        }
    }


    fun setupRecyclerView() {

        val layoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = layoutManager
        adapter = ProcessAdapter(requireContext(), list, this)
        recyclerView.adapter = adapter

    }


    override fun onItemSelect(position: Int, data: ProcessListDataItem) {
        //  goToActivity<CustomerDetailActivity>()
    }


    override fun onResume() {
        page = 1
        list.clear()
        hasNextPage = true
        swipeRefreshLayout.isRefreshing = true
        setupRecyclerView()
        recyclerView.isLoading = true
        getProcessList(page)
        super.onResume()
    }

    fun getProcessList(page: Int) {
        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("PageSize", Constant.PAGE_SIZE)
            jsonBody.put("CurrentPage", page)
            jsonBody.put("CustomerID", customerDataIteam?.customerID)
            jsonBody.put("VisitorID", customerDataIteam?.visitorID)
            result = Networking.setParentJsonData(
                    Constant.METHOD_GET_PROCRESS,
                    jsonBody
            )

        } catch (e: JSONException) {
            e.printStackTrace()
        }


        Networking
                .with(requireContext())
                .getServices()
                .getProcessList(Networking.wrapParams(result))//wrapParams Wraps parameters in to Request body Json format
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : CallbackObserver<ProcessListModal>() {
                    override fun onSuccess(response: ProcessListModal) {
                        if (list.size > 0) {
                            progressbar.invisible()
                        }
                        swipeRefreshLayout.isRefreshing = false
                        list.addAll(response.data)
                        adapter?.notifyItemRangeInserted(
                                list.size.minus(response.data.size),
                                list.size
                        )
                        hasNextPage = list.size < response.rowcount!!

                        refreshData(getString(R.string.no_data_found), 1)
                    }

                    override fun onFailed(code: Int, message: String) {
                        if (list.size > 0) {
                            progressbar.invisible()
                        }
                        // showAlert(message)
                        showAlert(getString(R.string.show_server_error))
                        refreshData(message, code)
                    }

                }).addTo(autoDisposable)
    }

    private fun refreshData(msg: String?, code: Int) {
        recyclerView.setLoadedCompleted()
        swipeRefreshLayout.isRefreshing = false
        adapter?.notifyDataSetChanged()

        if (list.size > 0) {
            imgNodata.invisible()
            recyclerView.visible()
        } else {
            imgNodata.visible()
            if (code == 0)
                imgNodata.setImageResource(R.drawable.no_internet_bg)
            else
                imgNodata.setImageResource(R.drawable.nodata)
            recyclerView.invisible()
        }
    }
}