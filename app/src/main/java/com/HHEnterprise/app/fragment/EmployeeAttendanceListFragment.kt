package com.HHEnterprise.app.fragment

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.HHEnterprise.app.R
import com.HHEnterprise.app.activity.AddAttendanceActivity
import com.HHEnterprise.app.adapter.EmpWiseAttendanceListAdapter
import com.HHEnterprise.app.extention.goToActivity
import com.HHEnterprise.app.extention.invisible
import com.HHEnterprise.app.extention.showAlert
import com.HHEnterprise.app.extention.visible
import com.HHEnterprise.app.interfaces.LoadMoreListener
import com.HHEnterprise.app.modal.EmployeeAttendanceListModel
import com.HHEnterprise.app.modal.EmployeeDataItem
import com.HHEnterprise.app.modal.EmployeeWiseDataItem
import com.HHEnterprise.app.network.CallbackObserver
import com.HHEnterprise.app.network.Networking
import com.HHEnterprise.app.network.addTo
import com.HHEnterprise.app.utils.Constant
import com.HHEnterprise.app.utils.SessionManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.reclerview_swipelayout.*
import org.json.JSONException
import org.json.JSONObject


class EmployeeAttendanceListFragment() : BaseFragment(),
        EmpWiseAttendanceListAdapter.OnItemSelected {
    constructor(empData: EmployeeDataItem?) : this() {
        this.empItemData = empData
    }

    private val list: MutableList<EmployeeWiseDataItem> = mutableListOf()
    var page: Int = 1
    var hasNextPage: Boolean = true
    var empItemData: EmployeeDataItem? = null
    var adapter: EmpWiseAttendanceListAdapter? = null
    var b: Boolean? = true

    lateinit var chipArray: ArrayList<String>
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_emp_attedance_wise_list, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        page = 1
        list.clear()
        hasNextPage = true
        swipeRefreshLayout.isRefreshing = true
        setupRecyclerView()
        recyclerView.isLoading = true
        getAttendenceList(page)

        recyclerView.setLoadMoreListener(object : LoadMoreListener {
            override fun onLoadMore() {
                if (hasNextPage && !recyclerView.isLoading) {
                    progressbar.visible()
                    getAttendenceList(++page)
                }
            }
        })

        swipeRefreshLayout.setOnRefreshListener {
            page = 1
            list.clear()
            hasNextPage = true
            recyclerView.isLoading = true
            adapter?.notifyDataSetChanged()
            getAttendenceList(page)
        }

    }


    fun setupRecyclerView() {

        val layoutManager = LinearLayoutManager(requireContext())
        recyclerView.layoutManager = layoutManager
        adapter = EmpWiseAttendanceListAdapter(requireContext(), list, this)
        recyclerView.adapter = adapter

    }

    override fun onItemSelect(position: Int, data: EmployeeWiseDataItem) {

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.home, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_add -> {
                goToActivity<AddAttendanceActivity>()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    fun getAttendenceList(page: Int) {
        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("PageSize", Constant.PAGE_SIZE)
            jsonBody.put("CurrentPage", page)
            jsonBody.put("UserID", empItemData?.userID)
            jsonBody.put("CityID", session.getDataByKey(SessionManager.KEY_CITY_ID))

            result = Networking.setParentJsonData(
                    Constant.METHOD_ADD_EMPLOYEE_WISE_ATTENDENCE,
                    jsonBody
            )

        } catch (e: JSONException) {
            e.printStackTrace()
        }

        Networking
                .with(requireContext())
                .getServices()
                .getEmpListWiseAttendance(Networking.wrapParams(result))//wrapParams Wraps parameters in to Request body Json format
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : CallbackObserver<EmployeeAttendanceListModel>() {
                    override fun onSuccess(response: EmployeeAttendanceListModel) {
                        if (list.size > 0) {
                            progressbar.invisible()
                        }
                        swipeRefreshLayout.isRefreshing = false

                        if (response.error == 200) {
                            list.addAll(response.data)
                            adapter?.notifyItemRangeInserted(
                                    list.size.minus(response.data.size),
                                    list.size
                            )
                            hasNextPage = list.size < response.rowcount!!
                        }

                        refreshData(getString(R.string.no_data_found), 1)
                    }

                    override fun onFailed(code: Int, message: String) {
                        if (list.size > 0) {
                            progressbar.invisible()
                        }
                        // showAlert(message)
                        showAlert(getString(R.string.show_server_error))
                        refreshData(message, code)
                    }

                }).addTo(autoDisposable)
    }


    private fun refreshData(msg: String?, code: Int) {
        recyclerView.setLoadedCompleted()
        swipeRefreshLayout.isRefreshing = false
        adapter?.notifyDataSetChanged()

        if (list.size > 0) {
            imgNodata.invisible()
            recyclerView.visible()
        } else {
            imgNodata.visible()
            if (code == 0)
                imgNodata.setImageResource(R.drawable.no_internet_bg)
            else
                imgNodata.setImageResource(R.drawable.nodata)
            recyclerView.invisible()
        }
    }
}